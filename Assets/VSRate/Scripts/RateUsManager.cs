﻿
/***********************************************************************************************************
 * Produced by App Advisory - http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/




using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AppAdvisory.VSRATE
{
	public class RateUsManager : MonoBehaviour 
	{
		#if APPADVISORY_ADS


		#if UNITY_ANDROID

		#if !VS_UI
		bool isAmazon
		{
		get
		{
		return AdsManager.instance.adIds.isAmazon;
		}
		}
		#else
		bool isAmazon
		{
		get
		{
		return FindObjectOfType<UIController>().isAmazon;
		}
		}
		#endif

		#else
		bool isAmazon = false;
		#endif



		#else
		#if !VS_UI
		public bool isAmazon = false;
		#else
		bool isAmazon
		{
		get
		{
		return FindObjectOfType<UIController>().isAmazon;
		}
		}
		#endif
		#endif
	

		#if !VS_UI
		/// <summary>
		/// URL of the iOS game. Find it on iTunes Connect.
		/// </summary>
		public string iD_iOS = "1134939249";
		public string url_ios
		{
			get
			{
				return "https://itunes.apple.com/us/app/id" + iD_iOS; //1134939249
			}
		}
		/// <summary>
		/// URL of the Android game. Find it on Google Play.
		/// </summary>
		public string bundleIdAndroid = "com.appadvisory.ab2";
		public string url_android
		{
			get
			{
				return "https://play.google.com/store/apps/details?id=" + bundleIdAndroid; //1134939249
			}
		}
		/// <summary>
		/// URL of the Amazon game. Find it on the Amazon Developer Console.
		/// </summary>
		public string amazonID = "B01DPBSF2A";
		public string url_amazon
		{
			get
			{
				return "https://www.amazon.fr/dp/" + amazonID; //1134939249
			}
		}

		public string URL_STORE
		{
			get
			{
				string URL = "";

		#if UNITY_IOS
				URL = url_ios;
		#else
		URL = url_android;
		if(isAmazon)
		URL = url_amazon;
		#endif

				return URL;
			}
		}

		#endif

		public GameObject buttonRateUs;
		public GameObject buttonWriteUs;
		public GameObject panel;

		public float numberOfStarsToAcceptReview = 3.5f;

		public string email = "me@example.com";
		public string subject = "My Subject";
		public string body = "My Body\r\nFull of non-escaped chars";

		static RateUsManager self;

		void Awake()
		{
			panel.SetActive(false);
		}

		void Start()
		{
			self = this;

			StarsManager.onNewratingEvent += onNewratingEvent;

			buttonRateUs.SetActive(false);
			buttonWriteUs.SetActive(false);
		}

		void onNewratingEvent(int num)
		{
			panel.SetActive(true);

			float rating = (float)(num + 1) / 2f;

			buttonRateUs.SetActive(rating >= numberOfStarsToAcceptReview);
			buttonWriteUs.SetActive(rating < numberOfStarsToAcceptReview);
		}

		public static void ShowRateUs(bool show)
		{
			print("ShowRateUs ; " + show);
			self.panel.SetActive(show);
		}

		public static void ShowRateUsWindows()
		{
			ShowRateUs(true);
		}

		public static void HideRateUsWindows()
		{
			ShowRateUs(false);
		}

		public static bool RateUsIsVisible()
		{
			return self.panel.activeInHierarchy;
		}

		public static void OpenRateUsURL()
		{
			Application.OpenURL(self.URL_STORE);
		}

		public static void SendEmail()
		{
			string url = "mailto:" + MyEscapeURL(self.email) + "?subject=" + MyEscapeURL(self.subject) + "&body=" + MyEscapeURL(self.body);

			print("url to open : " + url);

			Application.OpenURL(url);
		}

		static string MyEscapeURL (string url)
		{
			return WWW.EscapeURL(url).Replace("+","%20");
		}
	}
}