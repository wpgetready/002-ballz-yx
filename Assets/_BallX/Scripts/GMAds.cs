﻿using GoogleMobileAds.Api;

/// <summary>
/// 20170705: First version of class handling Ads. I'm trying to be as simple as posible.
/// </summary>
public static class GMAds {

    public static bool DebugAds = false;  //True when testing, false when not DON'T FORGET IT!!!
    public static string TEST_DEVICE = "A33D58161467384F7451243DEAF47AF4"; //test device, check it virtual machine or device using adb logcat to get this value!
    public static string ANDROID_BANNER = "ca-app-pub-3654628576200837/7912183231";
    public static string ANDROID_INTERSTITIAL = "ca-app-pub-3654628576200837/9388916439";
    public static string ANDROID_VIDEOREWARD = "ca-app-pub-3654628576200837/1865649636";

    /// <summary>
    /// Request a banner, return BannerView object (used to handle events if we want)
    /// </summary>
    /// <returns></returns>
    public static BannerView RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = ANDROID_BANNER;
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        BannerView bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom); // Create a 320x50 banner at the bottom of the screen.
        AdRequest request = getRequest(); // Create an empty ad request.
        bannerView.LoadAd(request); // Load the banner with the request.
        bannerView.Hide();
        return bannerView;
    }

    /// <summary>
    /// Call for interstitial, return InterstitialAd object (used to handle events if needed)
    /// Since this method returns an object, it can't control displaying it, something
    /// the caller needs to solve.
    /// </summary>
    /// <returns></returns>
    public static InterstitialAd RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = ANDROID_INTERSTITIAL;
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        InterstitialAd interstitial = new InterstitialAd(adUnitId);         // Initialize an InterstitialAd.
        AdRequest request = getRequest(); // Create an empty ad request.
        interstitial.LoadAd(request); // Load the interstitial with the request.
        return interstitial;
    }

    public static RewardBasedVideoAd RequestVideoReward()
    {
#if UNITY_ANDROID
        string adUnitId = ANDROID_VIDEOREWARD;
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif
        RewardBasedVideoAd rewardBasedVideoAd = RewardBasedVideoAd.Instance;
        AdRequest request = getRequest();
        rewardBasedVideoAd.LoadAd(request, adUnitId);
        return rewardBasedVideoAd;
    }

    /// <summary>
    /// Get a request depending of debug flag. Same task for every kind of ad
    /// </summary>
    /// <returns></returns>
    private static AdRequest getRequest()
    {

        AdRequest request;
        if (DebugAds)
        {
            request = new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
            .AddTestDevice(TEST_DEVICE)  // My test device.
            .Build();
        }
        else
        {
            request = new AdRequest.Builder().Build();
        }

        return request;
    }
}
