﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
/// <summary>
/// Basics for logging, scoring and retrieveing achievements from Google Play Games.
/// </summary>
public static class GPGames {
    public static bool authenticated = false;
    public static ArrayList achievementCompleted = new ArrayList();

    public static void GoogleGamesLogin()
    {
        PlayGamesPlatform.Activate();
        //The GPGSids class has to be defined from GooglePlayGames configuration
        ((PlayGamesPlatform)Social.Active).SetDefaultLeaderboardForUI(GPGSIds.leaderboard_ballz_yx_high_scores);
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate(tellmeWhenReady);
            return;
        }
        Debug.Log("Already authenticated");
    }

    static void tellmeWhenReady(bool success)
    {
        if (success)
        {
            Debug.Log("Autentication successful");
            authenticated = true;
        }
        else
        {
            Debug.Log("Autentication failed!");
            authenticated = false;
        }
    }

    public static void reportScore(int score)
    {
        if (authenticated)
        {
            Social.ReportScore(score, GPGSIds.leaderboard_ballz_yx_high_scores, (bool success) => { Debug.Log("Score reported"); });
        }
    }

    public static void showLeaderboard ()
    {
        if (authenticated)
        {
            Debug.Log("Displaying leaderboard...");
            Social.ShowLeaderboardUI();
            return;
        }
        Debug.Log("Show Leaderboard: not autenticathed");
    }

    public static void showAchievement()
    {
        if(authenticated)
        {
            Debug.Log("Displaying achievements...");
            Social.ShowAchievementsUI();
            return;
        }
        Debug.Log("Show achievement: not authenticated");
    }

    /// <summary>
    /// Checks achievement depending on the level.
    /// The arraylist is used to avoid sending achievement several times.
    /// </summary>
    /// <param name="level"></param>
    public static void checkAchievement(int level)
    {
       // Debug.Log(string.Format("check Ach {0}", level));
        string ach = "";
        switch (level)
        {
            case 1:
                if (!achievementCompleted.Contains(1))
                {
                    ach = GPGSIds.achievement_level_1;
                    achievementCompleted.Add(1);
                }
                break;
            case 5:
                if (!achievementCompleted.Contains(5))
                {
                    ach = GPGSIds.achievement_level_5;
                    achievementCompleted.Add(5);
                }
                break;

            case 10:
                if (!achievementCompleted.Contains(10))
                {
                    ach = GPGSIds.achievement_level_10;
                    achievementCompleted.Add(10);
                }
                break;

                //Test para acelerar el proceso de debuggeado.
            case 50:
                Debug.Log("empezando Ach...");
                if (!achievementCompleted.Contains(50))
                {
                    Debug.Log("Nivel 50");
                    ach = GPGSIds.achievement_level_50;
                    Debug.Log("Completando...");
                    achievementCompleted.Add(50);
                    Debug.Log("breaking...");
                }
                break;

            case 100:
                if (!achievementCompleted.Contains(100))
                {
                    ach = GPGSIds.achievement_level_100;
                    achievementCompleted.Add(100);
                }
                break;

            case 200:
                if (!achievementCompleted.Contains(200))
                {
                    ach = GPGSIds.achievement_level_200;
                    achievementCompleted.Add(200);
                }
                break;

            case 500:
                if (!achievementCompleted.Contains(500))
                {
                    ach = GPGSIds.achievement_level_500;
                    achievementCompleted.Add(500);
                }
                break;

            case 750:
                if (!achievementCompleted.Contains(750))
                {
                    ach = GPGSIds.achievement_level_750;
                    achievementCompleted.Add(750);
                }
                break;

            case 1000:
                if (!achievementCompleted.Contains(1000))
                {
                    ach = GPGSIds.achievement_level_1000;
                    achievementCompleted.Add(1000);
                }
                break;

            case 1500:
                if (!achievementCompleted.Contains(1500))
                {
                    ach = GPGSIds.achievement_level_1500;
                    achievementCompleted.Add(1500);
                }
                break;

            case 3000:
                if (!achievementCompleted.Contains(3000))
                {
                    ach = GPGSIds.achievement_level_3000;
                    achievementCompleted.Add(3000);
                }
                break;
            default:
                break;
        }
        if (ach!="")
        {
            if (authenticated)
            {
                //Debug.Log(string.Format("Reporting achievement {0}", ach));
                //  Debug.Log("Achievement:");
                //  Debug.Log(ach);
                Debug.Log("Reporting...");
                Social.ReportProgress(ach, 100.0f, (bool success) => { Debug.Log("Achievement reported"); });

            }
        }
    }
}
