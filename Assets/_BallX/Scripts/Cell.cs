﻿using UnityEngine;
using System.Collections;
using AppAdvisory.Utils;

namespace AppAdvisory.BallX 
{
	public class Cell : MonoBehaviour, IHitableByBall {

		[SerializeField] private SpriteRenderer spriteRenderer;
		[SerializeField] private TextMesh number;

		private Vector3 startScale;

		public event OnDestroyedEventHandler OnDestroyedByBall;

		public int _count;
		public int Count 
		{
			get 
			{
				return _count;
				//return int.Parse(number.text);
			}
			set 
			{
				_count = value;
				number.text = _count.ToString ();
			}
		}


		public Color Color 
		{
			get 
			{
				return spriteRenderer.color;
			}
			set 
			{
				spriteRenderer.color = value;
			}
		}


		public void SetCount(int count) {

			number.text = count.ToString ();
		}

		private int colorStep;
		private Color[] colors;
		public void SetColors(Color[] colors, int colorStep) 
		{
			this.colors = colors; 
			this.colorStep = colorStep;
			Color = GetColorFromCount (_count);
		}

		private Color GetColorFromCount(int count) {
			Color color;
			int max;
			for (int i = 0; i < colors.Length-1; i++) 
			{
				max = (i + 1) * colorStep;
				if (count < max) 
				{
					color = Color.Lerp (colors [i], colors [i + 1], (float) count/colorStep);
					return color;
				} 
			}
			color = colors [colors.Length - 1];
			return color;
		}

		private void Awake() {
			startScale = spriteRenderer.transform.localScale;

		}

		public IEnumerator DOPunchScaleCoroutine(float amplitude, float time = 1f) 
		{
			Vector3 midScale = startScale * (1 - amplitude);

			float count = 0;
			float firstDuration = time / 2;

			while (count < firstDuration) {
				count += Time.deltaTime;

				spriteRenderer.transform.localScale = Vector3.Lerp (startScale, midScale, count / firstDuration);
				yield return null;
			}

			count = 0;

			while (count < firstDuration) {
				count += Time.deltaTime;

				spriteRenderer.transform.localScale = Vector3.Lerp (midScale, startScale, count / firstDuration);
				yield return null;
			}

			spriteRenderer.transform.localScale = startScale;
		}




		public void BallHit (Ball ball)
		{
			//Count--;
			_count--;
			number.text = _count.ToString ();
			Color = GetColorFromCount (_count);
			StartCoroutine (DOPunchScaleCoroutine (0.1f, 0.1f));

			if (Count <= 0) {
				if(OnDestroyedByBall != null)
					OnDestroyedByBall(this);


                //Ok el truco ya lo he visto en otros juegos, asi que tengo que ingeniarme.
                //La idea es asi: cuando llega el momento de desaparecer la pieza, esta debería desaparecer de inmediato, pero ANTES:
                //Se reemplaza la pieza por otra, que puede ser invisible, del mismo tamaño que la pieza original. Esta pieza es la usada para
                //el efecto explosión, donde ningun objeto puede colisionar con ella.
                //La otra opción, a probar es cambiar el tag de inmediato, de manera que este objeto en adelante no pueda colisionarse.
                //So far so good con un problema: el objeto es visible, lo que genera efectos indeseados, como el contador en cero o negativo, etc.
                /*
                var exp = GetComponentInChildren<ParticleSystem>();

                if (exp !=null)
                {
                    var bum = Instantiate(gameObject);
                    bum.transform.rotation = transform.rotation;
                    bum.transform.position = transform.position;
                    Destroy(gameObject);
                    exp = bum.GetComponentInChildren<ParticleSystem>();
                    exp.Play();
                    Destroy(bum, exp.duration);

                    return;
                }
                */

				Destroy (gameObject);
			}
		}
	}
}

