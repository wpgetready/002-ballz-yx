﻿using UnityEngine;
using System;

namespace AppAdvisory.BallX
{
	public class OpenMoreGames : MonoBehaviour 
	{
		public void OnClicked()
		{
			Application.OpenURL(Constants.LINK_THE_KING_MOBILE);
		}
	}
}